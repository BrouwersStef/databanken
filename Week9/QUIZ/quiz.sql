SELECT count(*)
FROM employees
ORDER BY 1;

--age from birthdate
--conditionals ?
--if exists return else nothing ?


--1
--SUBQ

select avg(EXTRACT(YEAR FROM age(birth_date)))
from employees;

select department_id, sum(salary), avg(EXTRACT(YEAR FROM age(birth_date)))
from employees
group by department_id
having avg(EXTRACT(YEAR FROM age(birth_date))) > (select avg(EXTRACT(YEAR FROM age(birth_date))) from employees);


--2
--COR SUBQ
select max(salary)
from employees
group by department_id

SELECT salary
FROM employees e
WHERE salary =
      (SELECT MAX(salary)
       FROM employees
       WHERE department_id = e.department_id);

select department_name,
       first_name,
       last_name,
       salary
from employees e
         join departments d on e.department_id = d.department_id
where e.salary in (SELECT salary
                   FROM employees e
                   WHERE salary = (SELECT MAX(salary)
                                   FROM employees
                                   WHERE department_id = e.department_id))


select count(*)
from family_members
where upper(relationship) in ('SON', 'DAUGHTER')

select employee_id,
       (select count(*)
        from family_members
        where upper(relationship) in ('SON', 'DAUGHTER')
          and employee_id = e.employee_id)
from employees e

--where employee_id = ()

select *
from family_members
where employee_id = '999444444'

select count(*)
from family_members
where upper(relationship) in ('SON', 'DAUGHTER')
  and employee_id = '999444444'



--3
select t.employee_id,t.project_id,sum(hours)
from tasks t
join projects p on t.project_id = p.project_id
join employees e on t.employee_id = e.employee_id
where lower(p.project_name) not Like '%g%' and e.parking_spot > 400
group by t.employee_id,t.project_id
order by employee_id desc , project_id desc

select * from employees


select t.employee_id,t.project_id,parking_spot,project_name
from tasks t
         join projects p on t.project_id = p.project_id
         join employees e on t.employee_id = e.employee_id
where lower(p.project_name) not Like '%g%' and e.parking_spot > 400