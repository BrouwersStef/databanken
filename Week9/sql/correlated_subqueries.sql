INSERT INTO departments
VALUES(2,'Sales','999555555',CURRENT_DATE);

select * from departments

--8)
SELECT project_id,employee_id,hours
FROM tasks t
where hours < (select avg(hours)
              from tasks
              where project_id = t.project_id)
ORDER BY 1;
--9) !!!!!!!!!!!!!!!!!!!!!!!

SELECT manager_id, employee_id, salary
FROM employees e
WHERE salary = (SELECT max(salary)
                FROM employees
                WHERE manager_id = e.manager_id);

SELECT max(salary)
FROM employees
group by manager_id
--10)
select employee_id,first_name,last_name
from employees e
where EXISTS(
    select 'x'
    from employees
    where e.employee_id = manager_id
)
--11)
select department_id,department_name
from departments d
where Not exists(
    select 'x'
    from projects
    where d.department_id = department_id
    )
--12)
UPDATE employees
SET parking_spot=NULL
WHERE employee_id in ('999666666', '999887777');

select department_id
from employees e
where not exists
    (
    select 'x'
    from employees
    where parking_spot notnull
    and e.department_id = department_id
    )
