--1)
--  voordee --> snel
-- nadeel --> neemt opslag / porsessing power
--2)
-- no ( desided by querry optimiser)
--3A)
select *
from pg_indexes;
--3B)
select * from pg_indexes
where schemaname = 'lesW10'
and tablename = 'employees';
--4)
SELECT *
FROM employees
WHERE department_id=3;
--4A)
-- explain analyse
--4B)
explain analyse
SELECT *
FROM employees
WHERE department_id=3;
-- seq scan
--4C)
-- daalt

--5
create index idx_department_id on departments(department_id)

explain analyse
SELECT *
FROM employees
WHERE department_id=3;

--TIP: Je kan ook sequentieel scannen (= full range scan) ook uitzetten door gebruik te maken van volgende statement:
SET enable_seqscan TO off;
set enable_bitmapscan to off;

SET enable_seqscan TO on;
set enable_bitmapscan to on;



--6
explain analyse
SELECT *
FROM tasks
WHERE project_id=2;


explain analyse
SELECT *
FROM tasks
WHERE project_id=2;

-- bit heap scan
--7)
EXPLAIN
SELECT DISTINCT e.first_name, e.last_name
FROM employees e
         JOIN tasks t ON (e.employee_id = t.employee_id)
         JOIN projects p ON (t.project_id = p.project_id)
WHERE lower(p.location) LIKE '%e%'
  AND parking_spot IS NOT NULL;








