-- KdG email :          stef.brouwers@student.kdg.be
-- OS in use :          Windows
-- Date  :              2022/11/8
-- Period :             Periode 1 examen


/*
-------------
-- vraag 1 --
-------------

-- Toon per film categorie,
-- het aantal films met een category
        waarin de letters 'a' en 'o' zitten, waarbij de 'a' voor de 'o' komt (bv Action)
-- Toon enkel de categorien die meer dan 5 films bevatten.

OUTPUT
------

category   | movie count
-----------+------------
Action     |          64
Animation  |          66

(2 rows)
*/

----------------------
-- ANTWOORD VRAAG 1 --
----------------------
select c.name "category",count(f.title) "movie count"
from category c
         join film_category fc on c.category_id = fc.category_id
         join film f on fc.film_id = f.film_id
where lower(c.name) like '%a%o%'
group by category
having count(f.title) >5
order by "movie count";


/*
-------------
-- vraag 2--
-------------

Geef een lijst van alle films met de volgende criteria:
- ze hebben dezelfde huurprijs als films met een filmlengte van 166 minuten die "behind the scenes" special features hebben
- Maak alleen een lijst van films waarin de acteur Johnny Cage voorkomt
- de opgesomde films hebben een duur van minder dan 1 uur.

OUTPUT
------

title
-----------------
 Simon North
 Suspects Quills
(2 rows)
*/


----------------------
-- ANTWOORD VRAAG 2 --
----------------------
select title
from film f
join film_actor on f.film_id = film_actor.film_id
join actor a on film_actor.actor_id = a.actor_id
where concat_ws(' ',a.first_name,a.last_name) = 'Johnny Cage'
and f.rental_rate in(select rental_rate from film f where position('Behind the Scenes' in f.special_features)!=0 and f.length = 166)
and length < 60
order by title



/*
-------------
-- vraag 3 --
-------------
Geef voor alle film categorien die beginnen met de letters A tot F,
het aantal acteurs in die categorie
Gebruik geen tabellen die je niet nodig hebt!

OUTPUT
--------
CATEGORY     | NUMBER
-------------+--------
 Action      |     361
 Animation   |     0
 Children    |     307
 Classics    |     286
 Comedy      |     385
 Documentary |     0
 Drama       |     347
 Family      |     397
 Foreign     |     0
(9 rows)
*/
-----------------------------------------------------------------------------

----------------------
-- ANTWOORD VRAAG 3 --
----------------------

select c.name "CATEGORY",count(a.actor_id) "NUMBER"
from category c
         left join film_category fc on c.category_id = fc.category_id
         left join film_actor fa on fc.film_id = fa.film_id
         left join actor a on fa.actor_id = a.actor_id
where c.name like 'A%'or c.name like 'B%' or c.name like 'C%' or c.name like 'D%' or c.name like 'E%'or c.name like 'F%'
group by c.name
order by 1

