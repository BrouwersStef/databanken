select concat_ws(' ',a.first_name,a.last_name) "name actor",c.name"category name",count(c.name) "#amount"
from actor a
join film_actor fa on a.actor_id = fa.actor_id
join film f on fa.film_id = f.film_id
join film_category fc on f.film_id = fc.film_id
join category c on fc.category_id = c.category_id
group by 1,2
order by 1,3 desc

select concat_ws(' ',a.first_name,a.last_name) "name actor",count(title)
from actor a
         join film_actor fa on a.actor_id = fa.actor_id
         join film f on fa.film_id = f.film_id
group by 1
order by 2 desc
fetch next 5 row only
offset 1 row

select substring(cast(last_update as text) from length(cast(last_update as text))-7)
from film_actor

select distinct a .last_update,fa.last_update,f.last_update,l.last_update,fc.last_update,c.last_update
from actor a
full outer join film_actor fa on a.actor_id = fa.actor_id
full outer join film f on fa.film_id = f.film_id
full outer join language l on f.language_id = l.language_id
full outer join film_category fc on f.film_id = fc.film_id
full outer join category c on fc.category_id = c.category_id

select *
from actor a
full outer join film_actor fa on a.actor_id = fa.actor_id
    full outer join film f on fa.film_id = f.film_id
    full outer join language l on f.language_id = l.language_id
    full outer join film_category fc on f.film_id = fc.film_id
    full outer join category c on fc.category_id = c.category_id

-- what is this
select fulltext
from film
select special_features
from film
