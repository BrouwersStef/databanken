--oef1
select last_name, first_name, project_id
from employees
left outer join tasks t on employees.employee_id = t.employee_id
order by last_name asc
--oef2
select last_name, first_name
from employees
left outer join tasks t on employees.employee_id = t.employee_id
where t.project_id is null
--oef3
select last_name, first_name, count(project_id)
from employees
left outer join tasks t on employees.employee_id = t.employee_id
group by last_name, first_name
order by last_name
--oef4
select department_name, project_id, location
from departments
left join projects p on departments.department_id = p.department_id
--oef5A
select e.employee_id, last_name, department_name, name
from employees e
left join departments d on e.department_id = d.department_id
left join family_members fm on e.employee_id = fm.employee_id
order by last_name
--oef5B
select e.employee_id, last_name, department_name, name
from employees e
join departments d on e.department_id = d.department_id
left join family_members fm on e.employee_id = fm.employee_id
order by last_name
--oef6 ??? order ???
select last_name, first_name, project_name, sum(hours)
from employees e
left join tasks t on e.employee_id = t.employee_id
left join projects p on t.project_id = p.project_id
group by last_name, first_name, project_name
order by project_name
--oef7
select mgr.employee_id,mgr.last_name
    from employees e
right join employees mgr on e.manager_id=mgr.employee_id
where e.employee_id is null
order by 1
--oef8
select e.last_name, mgr.last_name--select e.last_name "has no mgr", mgr.last_name "does not manage"
from employees e
full outer join employees mgr on e.manager_id = mgr.employee_id
where e.last_name is null or mgr.last_name is null

--moet met full outer join oplossen





