--oef1
select concat_ws(' ',e.last_name,e.first_name) as "Employee FullName",'is managed by ' "-->",mgr.last_name,'and is related to'"-->",fm.name||' ('||fm.relationship||')' as "Family"
from employees e
left join employees mgr on e.manager_id = mgr.employee_id
left join family_members fm on e.employee_id = fm.employee_id
where mgr.last_name!='Jochems' or mgr.last_name is null
order by mgr.last_name,e.last_name

-- BIJ LEFT OUTER JOIN EN JE FILTERT ON RIGHT SIDE VALLEN NULL RIJEN OOK WEG
-- 'A' != 'B' --> TRUE
-- 'A' != NULL --> ONBEPAALD or FALSE

--oef2 mine
select concat(count(t.project_id),' employees') "#employees",concat('perform tasks on ',p.project_name) "project",concat('which is supported by dept. ',d.department_name) "department"
from tasks t
inner join projects p on t.project_id = p.project_id
join departments d on p.department_id = d.department_id
group by p.project_name, d.department_name
having sum(hours) >30
order by p.project_name