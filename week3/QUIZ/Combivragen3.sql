--oef1
select project_name "project name",e.employee_id EMP,first_name,last_name,hours,birth_date,date_part('year',age(birth_date))
from employees e
join tasks t on e.employee_id = t.employee_id
join projects p on t.project_id = p.project_id
where e.employee_id in('999111111','999222222','999555555') and hours > 6
order by hours
fetch first 3 row only;
--oef2
select concat_ws(' ',e.first_name,e.infix,e.last_name) "%name%",name,relationship,fm.gender,e.manager_id employee_id,upper(mgr.first_name) first_name_capitals,upper(mgr.last_name),mgr.birth_date
from employees e
join family_members fm on e.employee_id = fm.employee_id
JOIN employees mgr ON mgr.employee_id=e.manager_id
where upper(fm.gender)='F'
and mgr.birth_date>'1985-01-31'
order by e.last_name;



