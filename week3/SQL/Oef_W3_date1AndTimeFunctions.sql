--oef1
select distinct employee_id
from family_members
where date_part('year',age(birth_date)) < 18 --if 1 arg age() verschil tussen vandaag
--oef2
select employee_id,last_name,location,age(birth_date)
from employees
where date_part('year',age(birth_date)) > 30
and initcap(location)in('Maarsen','Eindhoven')
--oef3
select employee_id,age(birth_date)
from family_members
where date_part('year',age(birth_date))between 35 and 45
--oef4
select first_name,last_name, to_char(birth_date + interval '65 years','day DD month YYYY') as pens
from employees
order by birth_date + interval '65 years' desc
--oef5A
select name,to_char(birth_date,'day DD month YYYY')
from family_members
order by to_char(birth_date,'YYYY') desc
--oef5B
select name,trim(to_char(birth_date,'FMday FMDD FMmonth FMYYYY'))
from family_members
order by to_char(birth_date,'YYYY') desc
--oef5C
set lc_time = 'fr_FR';
select name,trim(to_char(birth_date,'TMday TMDD TMMonth TMYYYY'))
from family_members
order by to_char(birth_date,'YYYY') desc

