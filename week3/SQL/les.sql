--inner join = join
--aka inner join is default join


select employee_id, last_name, first_name, e.department_id, department_name -- department id is ambigieus
from employees as e --alias voor tabel
         inner join departments d on e.department_id = d.department_id
order by employee_id;


--join met using()
select employee_id, last_name, first_name, department_id, department_name -- weet uit welke tabel dept_id komt
from employees
         join departments using (department_id);
--werkt alleen als de TWEE kollomen exact hetzelfde noemen

--join met where
select employee_id, last_name, first_name, lower(gender), e.department_id, department_name
from employees e
         join departments d on e.department_id = d.department_id --eerst join dan where
where upper(gender) = 'F';

--join met using() en where
select employee_id, last_name, first_name, department_id, department_name
from employees
         join departments using (department_id)
where upper(department_name) = 'PRODUCTION';


select e.employee_id, e.last_name, p.project_name, t.project_id, t.hours
from employees e
         join tasks t on e.employee_id = t.employee_id --Order van joins is belangerijk
         join projects p on t.project_id = p.project_id;

--*********
--auto join
--*********

select e.employee_id,e.first_name,e.last_name,' manager is ' " ",mgr.last_name
from employees e
         join employees mgr on e.manager_id = mgr.employee_id
where e.employee_id='999444444';

select e.last_name
from employees e
join  employees mgr on (mgr.employee_id=e.manager_id)
where upper(mgr.last_name) ='BORDOLOI'

