--oef1 ??????????????????????????
select jochems.last_name "last_name Jochems",jochems.location "city Jochems",emp_male.last_name "last_name",emp_male.location "city"
from employees emp_male
inner join employees jochems on emp_male.location != jochems.location --NIET JOINEN OP EMP_ID
where emp_male.gender='M'
and jochems.last_name='Jochems'
order by last_name
--oef2 ORDER BY NIET GOED
select e2.employee_id,e2.last_name,e2.birth_date
from employees e1
join employees e2 on to_char(e1.birth_date,'month')=to_char(e2.birth_date,'month')
where e1.employee_id!=e2.employee_id
order by to_char(e1.birth_date,'month') asc,to_char(e1.birth_date,'day') asc
--oef3
select p2.project_id,p2.project_name,p2.location,p2.department_id
from projects p1
join projects p2 on p1.project_id=p2.project_id
where p1.department_id='7'and p1.project_id!='3'
--oef4
SELECT e.last_name,mgr.last_name,mgr2.last_name
FROM employees e
JOIN employees mgr ON (mgr.employee_id=e.manager_id)
join employees mgr2 ON (mgr2.employee_id=mgr.manager_id)
