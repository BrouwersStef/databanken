--oef1
SELECT a.department_id, department_name, project_id, project_name, location
FROM DEPARTMENTS a
inner join projects p on a.department_id = p.department_id
order by a.department_id;
--oef2
select distinct e.department_id,employee_id as manager_id,last_name,salary,parking_spot
from employees e
inner join departments d on e.employee_id = d.manager_id --manager_id in dep is emp_id in employees
order by department_id;
--oef3
select p.project_name,p.location,e.first_name||' '||e.last_name as full_name,e.department_id
from projects p
inner join tasks t on p.project_id = t.project_id
inner join employees e on e.employee_id = t.employee_id
order by department_id,full_name;
--oef4
select p.project_name,p.location,CONCAT(e.first_name||' ',e.infix||' ',e.last_name) as full_name,e.department_id,p.department_id
from projects p
         join tasks t on p.project_id = t.project_id
         join employees e on e.employee_id = t.employee_id
WHERE UPPER(p.location) = 'EINDHOVEN' OR p.department_id = 3
order by p.department_id,full_name;
--oef5
select first_name,last_name,count(project_id)as "number of PROJECTS"
from employees e
inner join tasks t on e.employee_id = t.employee_id
group by first_name, last_name
order by "number of PROJECTS";
--oef6
select CONCAT(e.first_name||' ',e.infix||' ',e.last_name) as full_name,fm.name,fm.gender,fm.birth_date
    from employees e
inner join family_members fm on e.employee_id = fm.employee_id
where fm.relationship='DAUGHTER' or fm.relationship='SON';
--oef 7
select concat_ws('',e.first_name,e.infix,e.last_name) as full_name,count(concat_ws('',e.first_name,e.infix,e.last_name))
from employees e
         inner join family_members fm on e.employee_id = fm.employee_id
where fm.relationship='DAUGHTER' or fm.relationship='SON'
group by full_name;
