--INDEX CORRELATED SUBQUERRY

--                                  INDEX
--Enkelvoudige <> Samengestelde index:
--één/meer attributen per index entry

--Unieke index:
--bij unieke index zijn meerdere rijen met dezelfde indexwaardes niet toegestaan

--• Gewone index
CREATE INDEX ind_emp_department ON employees (department_id);
--• Unieke index
CREATE UNIQUE INDEX ind_dep_name ON departments (department_name);
--• Samengestelde gewone index (=composite/concatenated key)
CREATE INDEX ind_emp_sal_dep ON employees (salary, department_id);
--• Samengestelde unieke index (voorbeeld met PK)
CREATE TABLE tasks
(
    employee_id,…,
    CONSTRAINT pk_tasks PRIMARY KEY(employee_id,project_id)
    …);

explain analyze
--• Seq scan: alle rijen één voor één aflopen = sequentieel • Cost = indicatie van werklast (startkost .. totale kost) • Rows = schatting van het aantal output rijen • Width = breedte van de output rijen (in bytes)
--• Planning time: tijd dat het systeem nodig heeft om te beslissen
--HOE de query zal uitgevoerd worden.
--• Execution time: effectieve uitvoeringstijd van de query (zonder planning time mee te tellen).


--                                  corr subq
--                                  EXISTS

--EXISTS wordt niet voorafgegaan door een constante, attribuut of expressie
--  In de subselect kan je schrijven : select ‘x’ omdat we niet geïnteresseerd zijn in de inhoud van de select, maar wel in het feit of de subquery al dan niet rijen oplevert
--  De subquery levert geen rijen op maar WAAR of ONWAAR
--   EXISTS wordt steeds gebruikt in combinatie met een correlated subquery

SELECT
FROM
    WHERE [NOT] EXISTS (correlated subquery)


select employee_id, first_name, last_name
from employees e
where EXISTS(
              select 'x'
              from employees
              where e.employee_id = manager_id
          )