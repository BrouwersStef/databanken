--DDL ALTER DROP SEQ DML

--                                  INSERT
-- alle attributen krijgen een waarde toegewezen
INSERT INTO departments(department_id, department_name, manager_id, mgr_start_date)
VALUES (5, 'Sales', '999444444', to_date('22-05-2019', 'DD-MM-YYYY'));
--NIET alle attributen krijgen een waarde toegewezen
INSERT INTO departments(department_id, department_name)
VALUES (5, 'Sales');
--                                  UPDATE
--                         1 tabel
--UPDATE tabelnaam
--SET attribuutnaam=nieuwe waarde
--WHERE conditie;
--                       2 tabellen
--UPDATE tabelnaam
--SET attribuutnaam =nieuwe waarde
--FROM tabelnaam2
--WHERE joinconditie
--AND conditie;
--                     3+ tabellen
--UPDATE tabelnaam
--SET attribuutnaam=nieuwe waarde
--FROM tabelnaam2
--JOIN tabelnaam 3
--WHERE joinconditie
--  AND conditie;
--                                  DELETE
--delete
DELETE FROM tabelnaam
WHERE conditie;
--DELETE (met USING – postgreSQL Syntax)
DELETE FROM tabelnaam
    USING tabelnaam2
WHERE joinconditie
  AND conditie;
--DELETE WHERE CLAUSE
DELETE FROM departments
WHERE department_id > 9;
--                                  TRUNCATE
TRUNCATE TABLE tabelnaam;
--                                  DROP TABLE
DROP TABLE [IF EXISTS] tabelnaam
    [CASCADE | RESTRICT];
--                                  ALTER TABLE
--                        ADD / DROP
ALTER TABLE [IF EXISTS] tabelnaam + combinatie van volgende
    ‘actions’
▪ ADD (COLUMN | TABLE_CONSTRAINT)
▪ DROP [IF EXISTS] (COLUMN | CONSTRAINT)
▪ ALTER (COLUMN)
▪ VALIDATE (CONSTRAINT)
▪ RENAME (COLUMN | CONSTRAINT)
--ADD COLUMN
ALTER TABLE employees2
    ADD COLUMN food_allergy VARCHAR(30);
--ADD CONSTRAINT
ALTER TABLE employees
    add constraint ch_dd check (last_name = upper(last_name));
--DROP COLUMN
ALTER TABLE employees2
    DROP COLUMN food_allergy;
--DROP CONSTRAINT
ALTER TABLE employees
    drop constraint ch_dd check (last_name = upper(last_name) )
--                        ALTER / VALIDATE / RENAME
--alter
ALTER TABLE [IF EXISTS] tabelnaam
ALTER [COLUMN] attribuutnaam

[SET DATA] TYPE gegevenstype ;
SET DEFAULT expressie ;
DROP DEFAULT ;
{SET | DROP} NOT NULL;
--validate
ALTER TABLE [IF EXISTS] tabelnaam
    VALIDATE CONSTRAINT constraintnaam
--rename
ALTER TABLE [IF EXISTS] tabelnaam
    { RENAME TO new_table_name
    | RENAME [COLUMN] columnname TO new_columnname
    | RENAME CONSTRAINT constraintname TO new_constraintname
    }
-- CONSTRAINT WIJZIGEN
ALTER TABLE employees2
    DROP CONSTRAINT ch_emp2_lastname,
    ADD CONSTRAINT ch_emp2_lastname
        CHECK(last_name=INITCAP(last_name)) ;
--                        SEQUENCES
--create
CREATE SEQUENCE [IF NOT EXISTS] sequencenaam
    INCREMENT [BY ] increment
    [MINVALUE minvalue | NO MINVALUE]
    [MAXVALUE maxvalue| NO MAXVALUE]
    [START [WITH] startwaarde
[CACHE cache]
[[NO] CYCLE]
--alter
ALTER SEQUENCE seq_project_id NO MAXVALUE;
-- (no) minvalue
-- (no) maxvalue
--drop
DROP SEQUENCE [ IF EXISTS ] sequencenaam
--functies
--Nextval ( sequencenaam) → bigint
--Currval (sequencenaam) → bigint
--Setval( sequencenaam, newvalue , iscalled) → bigint
--                        IDENTITY COLLUM
-- auto matish gegenereerd als pk
CREATE TABLE tabelnaam
attribuutnaam GENERATED ALWAYS( by default) AS IDENTITY


create table emp
(
    employee_id INTEGER
        generated always as identity
        constraint pk primary key,
    last_name varchar(25) not null
)




