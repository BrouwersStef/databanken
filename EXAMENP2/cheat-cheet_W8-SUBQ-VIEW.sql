--subquerry en vieuws

--                                  SUBQUERY SUBQ
--subquerry kunnen in select, from, where en having
--subq die 1 rij opleveren
-- = > < >= <= !=
--subq die meerdere rijen kunnen opleveren
--IN, ANY en ALL
--                                  VIEUWS
--view kan gegevens van de onderliggende tabel afschermen
CREATE VIEW v_emp_parking_spot
AS
    SELECT parking_spot, employee_id,first_name, last_name
    FROM EMPLOYEES
    ORDER BY 1;
--      onmogelijk in vieuw
--  creatie
--group by
--distinct
--statistishe functies
--  instert/update
--een pseudokolom bevat (bv SYSDATE, seq_.....NEXTVAL of CURRVAL)
--  instert
--als de vieuw niet alle not null van onderliggende tabel bevat
--      Een view is updatable indien de view definitie:
--• Exact 1 element (tabel of andere updatable view) in de FROM lijst bevat
--• GEEN van volgende KEYWORDS bevat WITH, DISTINCT, GROUP BY, HAVING, LIMIT, or OFFSET
--• GEEN set operaties bevat UNION, INTERSECT or EXCEPT
--• de select bevat GEEN aggregates (count, …) of functies die een set teruggeven.

