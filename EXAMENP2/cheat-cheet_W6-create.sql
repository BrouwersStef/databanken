--DDL - Create Tables

--                                  GEGEVENSTYPE
--CHAR
--VARCHAR
--NUMERIC
--DATE
--TIME
--TIMESTAMP
--INTERVAL
--NUMERIC(N,M) --> n geeft de totale precisie weer | m geeft het aantal decimalen weer
--meer dan M decimalen worden afgerond naar M
--na afronding meer dan N tekens = foutmelding
--                                  DATUMS
--set datestyle to iso;select current_date; -- "2021-11-12"
--set datestyle to SQL;select current_date; -- "11/12/2021"
--set datestyle to postgres;select current_date; -- "11-12-2021
--                                  DATUMS
--                                  CONSTRAINT
--Check Constraints
--Not-Null Constraints (hebben geen naam)
--Unique Constraints
--Primary Keys
--Foreign Keys (refrence)
--Exclusion Constraints

--Je kan een beperking opleggen aan 1 attribuut (= column constraint) (= inline constraint)
--CONSTRAINT constraintnaam column-constraint
--Je kan een beperking opleggen aan een combinatie van attributen (= table constraint) (= out of line constraint)
--CONSTRAINT constraintnaam
--FOREIGN KEY(attr1,attr2,…)
--REFERENCES tabel (attri,attrj…)
--                                  KEY CONSTRAINT
--•Key constraint
--  »een primaire sleutel is uniek en blijft uniek
--•Entity integrity constraint
--  »de primaire sleutel moet steeds een geldige waarde
--  krijgen (d.w.z. een waarde verschillend van NULL)
--•Referential integrity constraint
--  »voor elke waarde van een vreemde sleutel bestaat er een
--  overeenkomstige waarde van de primaire sleutel in de
--  tabel waarnaar verwezen wordt

--Constraint bij creatie
CREATE TABLE employees
(
    employee_id CHAR(9)
        CONSTRAINT pk_employee_id PRIMARY KEY,
    first_name  VARCHAR(25)
        CONSTRAINT c_firstname
            CHECK (first_name = UPPER(first_name))
);
--constraint na creatie
--XXX

--default waardes
CREATE TABLE employees
(
    employee_id CHAR(9),
    start_date  date          DEFAULT current_date,
    first_name  VARCHAR(25)   default 'UNKONWN',
    salery      numeric(7, 2) default 10000
);

--2 primary keys
CREATE TABLE locations
(
    dept_id NUMERIC(2) CONSTRAINT fk_loc_dept_id REFERENCES departments,
    city VARCHAR(20) NOT NULL,

    CONSTRAINT pk_locations PRIMARY KEY (dept_id,city)
);
--ON DELETE / update
--Delete actions of rows in the parent table
--If you delete one or more rows in the parent table, you can set one of the following actions:

ON DELETE/UPDATE  NO ACTION: SQL Server raises an error and rolls back the delete action on the row in the parent table.

ON DELETE/UPDATE  CASCADE: SQL Server deletes the rows in the child table that is corresponding to the row deleted from the parent table.

ON DELETE/UPDATE  SET NULL: SQL Server sets the rows in the child table to NULL if the corresponding rows in the parent table are deleted. To execute this action, the foreign key columns must be nullable.

ON DELETE/UPDATE  SET DEFAULT SQL Server sets the rows in the child table to their default values if the corresponding rows in the parent table are deleted. To execute this action, the foreign key columns must have default definitions. Note that a nullable column has a default value of NULL if no default value specified.

By default, SQL Server applies ON DELETE NO ACTION if you don’t explicitly specify any action.