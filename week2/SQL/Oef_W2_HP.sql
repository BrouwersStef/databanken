--oef1
select distinct clientno
from reservations;
--oef2
select clientno " ", 'travelled on' " ", start_date " ", 'to park' " ", park_code " "
from reservations
where start_date > '2021-01-01'
order by start_date desc
    FETCH first 5 ROW ONLY;
--question
--oef3
select current_date "today it's";
--oef4
select resno, tano, clientno, end_date + 2 "ext 2 day"
from reservations
where park_code = 'MD';
--oef5
select tano, clientno, start_date, end_date
from reservations
where tano = '3'
order by start_date asc;
--oef6
select clientno, status
from reservations
where status = 'OPEN'
order by clientno desc
    fetch first % rows only
--oef7
select clientno, status "paid"
from reservations
where status = 'PAID'
  and start_date >= '2020-07-01';
--oef8
select *
from cottages
where houseno < 12
  and (playground = 'Y' or corner = 'Y')
order by park_code asc;
--oef9
select park_code, typeno, price_weekend "weekend", price_midweek "midweek"
from cottype_prices
where (price_midweek) > (price_weekend * 1.20);
--oef10
select park_name, country_code
from parks
where country_code in ('1', '2');
--oef11
select resno, park_code, typeno, houseno, status
from reservations
where (houseno is null or typeno is null ) and status = 'OPEN';
--oef12
select * from clients
where postcode in('2060','2100','2640');
--oef13
select * from clients
where (houseno='106' and postcode='2640')or (city LIKE 'A%' or city LIKE 'B%' or city LIKE 'C%' or city LIKE 'D%')
--oef14
select * from cottype_prices
where price_midweek<250 and (price_extra_day<30 or price_extra_day is null )
--oef15
select * from payments
where payment_method !='O' and payment_date < '01-02-2020'
--oef16
SELECT DISTINCT last_name
FROM clients
WHERE postcode = '2640'
ORDER BY clientno;
--collom moet geselecteerd zijn voor the kunnen orderen
--oef17
select * from cottages
where park_code='BF' and pet='N';
--oef18
select upper(country_name) from countries
where upper(country_name) like 'N%'
order by country_name
--oef19
select last_name, first_name , street from clients
where street like '%LAAN%'



