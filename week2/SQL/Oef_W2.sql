--oef1
select *
from projects;
--oef2
select project_name, department_id
from projects;
--oef3A
select 'project', project_name, 'is handled by', department_id
from projects;
--oef3B
select 'project' as " ", project_name, 'is handled by' as " ", department_id
from projects;
--oef3C
select 'project ' || project_id || ' is handled by ' || department_id as "projects with department"
from projects;
--oef4
SELECT current_date - birth_date
FROM FAMILY_MEMBERS;
--days old
--oef5A1
--no from
--oef5A2
--table task doens't exist
--oef5A3
-- start_date doens't exist
--oef5B
SELECT last_name, salary, department_id
FROM EMPLOYEES;
--no komma na salary
--oef6A
select distinct location
from employees;
--maastricht != Maastricht
--oef6B
select distinct initcap(location)
from employees;
--oef7
select distinct department_id, initcap(location)
from employees
order by initcap(location) desc;
--oef8A
select current_date as "Date";
--oef8B
select 150 * (1 - 0.15) as "calculation";
--oef8C
select 'SQL ' || 'Data retrieval ' || 'Chapter 3-4' as "Best course";
--oef9
select employee_id "employee", name "NAME FAMILY MEMBER", relationship, gender
from family_members
where employee_id = '999111111'
order by name;
--oef10
select *
from departments
where department_name = 'Administration';
--oef11
SELECT employee_id, last_name, location
FROM EMPLOYEES
WHERE initcap(location) = 'Maastricht';
--oef12
select employee_id, project_id, hours
from tasks
where project_id = '10'
  and hours between 20 and 35;
--oef13
select project_id, hours
from tasks
where employee_id = '999222222'
  and hours < 10;
--oef14(1)
select employee_id, last_name, province
from employees
where province = 'NB'
   or province = 'GR';
--oef14(2)
select employee_id, last_name, province
from employees
where province in ('GR', 'NB');
--oef15
select department_id, first_name
from employees
where first_name in ('Suzan', 'Martina', 'Henk', 'Douglas')
order by department_id desc;
--oef16
select last_name, salary, department_id --first_name || ' ' || last_name name
from employees
where department_id = '7' and salary < 40000
   or employee_id = '999666666';
-- geen samendehangende condities
--oef17
select last_name,department_id from employees
where initcap(location) not in('Maarssen','Eindhoven');
--oef18A
select * from tasks
order by hours asc nulls first;
--oef18B
select * from tasks
order by hours desc nulls last;
--oef19
select last_name,location,salary from employees
where (lower(location) Like 'm%' or lower(location) Like 'o%')and salary>30000;
--oef20
select name from family_members
where date_part('year',birth_date)='1988'