--oef1A
select first_name || ' ' || last_name as naam
from employees;
--oef1B
select concat_ws(' ', first_name, infix, last_name)
from employees;
--oef1C
select concat(rpad(first_name, length(first_name) + 1, ' '), last_name)
from employees;
--oef2A
select lower(street)
from employees
--oef2B
select trim(leading 'z' from lower(street))
from employees
order by street desc
--oef2C
select rpad(trim(leading 'z' from lower(street)), 30, '*')
from employees
--oef3
select first_name, last_name
from employees
where POSITION('o' in lower(first_name)) != 0
  and POSITION('o' in lower(last_name)) != 0
--oef4 ?amolsvoort
select last_name
from employees
where POSITION('oo' in lower(last_name)) != 0
  and POSITION('o' in lower(replace(last_name, 'oo', 'xx'))) = 0
--oef5
--p1
select left(street,position('e' in street))
from employees
--p2
select replace(substr(street,position('e' in street)),'e','o')
from employees
--opl
select concat(left(street,position('e' in street)),replace(substr(street,position('e' in street)+1),'e','o'))
from employees

--oef6
select concat(left(first_name, 3),'.', left(last_name, 3),'@', department_name, '.be') email
--select concat_ws('.', left(first_name, 3), left(last_name, 3)) || '@' || concat_ws('.', department_name, 'be') as email
from employees e
         join departments d on e.department_id = d.department_id
order by email asc




