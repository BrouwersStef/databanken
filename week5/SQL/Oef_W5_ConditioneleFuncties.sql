--oef1
SELECT e.employee_id,first_name,relationship,
Case
    when date_part('year',age(fm.birth_date)) >= 18 THEN 'Adult'
    when date_part('year',age(fm.birth_date)) < 18 THEN 'Child'
end
from employees e
join family_members fm on e.employee_id = fm.employee_id
where relationship in('SON','DAUGHTER')
--oef2 ???????
select replace(concat_ws(' ',first_name,infix,last_name),' ','/')
from employees
--oef3A
select DISTINCT e.employee_id,first_name,e.birth_date,
CASE
    WHEN upper(relationship) = 'PARTNER' THEN initcap(name)
    else 'Single'
end
from employees e
left join family_members fm on e.employee_id = fm.employee_id
where relationship = 'PARTNER' or relationship is null
--oef3B
select DISTINCT e.employee_id,first_name,e.birth_date,
                CASE
                    WHEN upper(relationship) = 'PARTNER' THEN initcap(name)
                    ELSE 'Single'
                    END,
    fm.birth_date,
                CASE
                    WHEN least(e.birth_date,fm.birth_date) = e.birth_date THEN initcap(e.first_name)
                    WHEN least(e.birth_date,fm.birth_date) = fm.birth_date THEN initcap(fm.name)
                    ELSE initcap(e.first_name)
                END
from employees e
         left join family_members fm on e.employee_id = fm.employee_id
where relationship = 'PARTNER' or relationship is null
--oef4A
select date_part('hours',localtime)*3600+date_part('minutes',localtime)*60+cast(to_char(date_part('second',localtime),'FM99')as int) "sec"
select round(extract(EPOCH from localtime)) "sec" -- EPOCH seconds sinse X
--oef4B

--oef4C

--oef4D

--oef4E

--oef4F





