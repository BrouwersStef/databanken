--oef1
select to_char(birth_date,'YYYY/MM/DD') as "birth date"
from employees
union all
select to_char(birth_date,'YYYY/MM/DD')
from family_members
order by "birth date";
--oef 2
select birth_date
from employees as a
union all
select birth_date as a
from family_members;
--oef3
select employee_id
from employees
except
select employee_id
from family_members
order by employee_id desc;
--oef4
select employee_id
from employees
except
select manager_id
from departments