-- Geef voor ALLE projectlocaties, per locatie het totaal aantal gepresteerde uren.
-- Zorg voor een duidelijk zicht op de status van een project.
--  een project onder de 50 uren heefst status 'Project on time',
--  een project tussen de 50 en 80 uren heeft status 'Project takes longer than expected'
--  een project met meer dan 80 gepresteerde uren heeft status 'Project overdue'
-- LET op: tabelnaam, volgorde en verwoording in de voorbeeldtabel.

-- location   |Totaal uren  |Project timing
-- -----------+-------------+----------------
-- Maastricht |  109.9      |Project overdue
-- Eindhoven  |  79.2       |Project takes longer than expected
-- Oegstgeest |  52.4       |Project takes longer than expected
-- Groningen  |  42.7       |Project on time
-- (4 rows)

select location,sum(hours),
CASE
WHEN sum(hours)<50.0 THEN 'Project on time'
WHEN sum(hours)>50 and sum(hours)<80 THEN 'Project takes longer than expected'
ELSE 'Project overdue'
END as "Project timing"
from projects
left join tasks t on projects.project_id = t.project_id
group by location
order by sum(hours) desc

-- Geef voor de medewerkers met een partner het leeftijdsverschil
-- weer en geef aan of de partner ouder of jonger is.

-- First_name | last_name   | older/younger      | age difference
-- -----------+-------------+--------------------+--------------
-- Suzan      |  Jochems    | Partner is younger | -3
-- Willem     |  Zuiderweg  | Partner is older   |  0
-- Douglas    |  Bock       | Partner is older   |  1
-- (3 rows)

select first_name,last_name,relationship,
       CASE
           WHEN date_part('year',age(e.birth_date)-age(fm.birth_date)) <0 THEN 'Partner is younger'
           ELSE 'Partner is older'
    END as "older/younger"
    ,date_part('year',age(e.birth_date)-age(fm.birth_date)) "age difference"
from employees e
right join family_members fm on e.employee_id = fm.employee_id
where upper(relationship) ='PARTNER'

--WHEN date_part('year',age(e.birth_date))-date_part('year',age(fm.birth_date)) <0 THEN 'Partner is younger'
--WHEN date_part('year',age(e.birth_date))-date_part('year',age(fm.birth_date)) =0 THEN 'even oud'
--WHEN date_part('year',age(e.birth_date))-date_part('year',age(fm.birth_date)) >0 THEN 'Partner is older'