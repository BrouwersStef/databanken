--dropper
drop table Travelagencies cascade;
drop table Clients cascade;
drop table Parks cascade;
drop table Seasons cascade;
drop table Parkattractiontypes cascade;
drop table Countries cascade;
drop table Parkattractions cascade;
drop table Cottagetypes cascade;
drop table Cottages cascade;
drop table Reservations cascade;
drop table Cottype_prices cascade;
drop table Promotions cascade;
drop table Payments cascade;

--oef

create table Travelagencies
(
    tano     numeric(2)
        constraint pk_tano primary key,
    ta_name  varchar(10)
        constraint HL_ta_name check ( ta_name = upper(ta_name) ),
    street   varchar(40)
        constraint HL_street check ( ta_name = upper(ta_name) ),
    houseno  varchar(5),
    postcode varchar(6),
    city     varchar(20)
        constraint HL_city check ( ta_name = upper(ta_name) )
);

create table Clients
(
    clientno   varchar(5)
        constraint pk_clientno primary key,
    last_name  varchar(25)
        constraint HL_LastName check ( last_name = upper(last_name) ),
    first_name varchar(25)
        constraint HL_FirstName check ( first_name = upper(first_name) ),
    street     varchar(40)
        constraint HL_street check ( street = upper(street) ),
    houseno    varchar(5),
    postcode   varchar(6),
    city       varchar(40)
        constraint HL_city check ( city = upper(city) ),
    status     varchar(10)
);
create table Countries
(
    country_code varchar(3),
    country_name varchar(50),
    tel_code     varchar(4),
    constraint pk_counrties primary key (country_code)
);
create table Parkattractiontypes
(
    attraction_code numeric(4)
        constraint pk_Attraction_code primary key,
    description     varchar(100)
);
create table Seasons
(
    code        numeric(2)
        constraint pk_code primary key,
    description varchar(30),
    start_date  date,
    end_date    date,
    CONSTRAINT integr_seas_dates CHECK (end_date >= start_date)
);
create table Parks
(
    Park_name    varchar(15)
        constraint HL_park_name check ( Park_name = upper(Park_name) ),
    sport        varchar(9),
    country_code varchar(3)
        constraint FK_country_code references countries (country_code),
    code         varchar(2)
        constraint HL_codepark check ( code = upper(code) )
        constraint Pk_park_code primary key
);
create table Parkattractions
(
    Park_code       varchar(2)
        constraint fk_park_code references Parks (code),
    attraction_code numeric(4)
        constraint fk_Attraction_code references parkattractiontypes (attraction_code),
    constraint pk_Parkattractions primary key (Park_code, attraction_code)
);
create table Cottagetypes
(
    park_code    varchar(2) unique
        constraint fk_park_code references parks (code),
    typeno       varchar(4) unique
        constraint HL_typeno check ( typeno = upper(typeno) ),
    No_bedrooms  numeric(1),
    No_persons   numeric(2),
    No_bathrooms numeric(1),
    cot          varchar(1)
        constraint check_y_n_cot check ( cot in ('Y', 'N')),
    comments     varchar(30),
    wifi         varchar(1)
        constraint check_y_n_wifi check ( wifi in ('Y', 'N')),
    surface      numeric(3),
    constraint pk_Cottagetypes primary key (park_code, typeno)
);
create table Cottages
(
    park_code  varchar(2)
        constraint fk_park_code references cottagetypes (park_code),
    typeno     varchar(4)
        constraint fk_typeno references cottagetypes (typeno),
    houseno    numeric(3) unique ,
    corner     varchar(1)
        constraint check_y_n_corner check ( corner in ('Y', 'N')),
    central    varchar(1)
        constraint check_y_n_central check ( central in ('Y', 'N')),
    pet        varchar(1)
        constraint check_y_n_pet check ( pet in ('Y', 'N')),
    quiet      varchar(1)
        constraint check_y_n_quite check ( quiet in ('Y', 'N')),
    playground varchar(1)
        constraint check_y_n_playground check ( playground in ('Y', 'N')),
    beach      varchar(1)
        constraint check_y_n_beach check ( beach in ('Y', 'N')),
    constraint pk_Cottages primary key (park_code, typeno, houseno)
);
create table Cottype_prices
(
    park_code       varchar(12)
        constraint fk_park_code_Cottype_prices references cottagetypes (park_code),
    typeno          varchar(4)
        constraint fk_typeno_Cottype_prices references cottagetypes (typeno),
    season_code     numeric(2)
        constraint fk_season_code_Cottype_prices references seasons (code),
    price_weekend   numeric(5),
    price_midweek   numeric(5),
    price_extra_day numeric(5),
    constraint pk_Cottype_prices primary key (park_code,typeno,season_code)
);
create table Promotions
(
    promo_code varchar(9)
        constraint pk_Promotions primary key,
    percentage numeric(3, 1),
    start_date date,
    end_date   date,
    park_code  varchar(2)
        constraint fk_park_code_Promotions references cottagetypes (park_code),
    typeno     varchar(4)
        constraint fk_typeno_Promotions references cottagetypes (typeno),
    constraint ck_checkdate check ( end_date >= start_date )
);
create table Reservations
(
    resno        numeric(4),
    tano         numeric(2)
        constraint fk_tano_res references travelagencies (tano),
    cientno      varchar(5)
        constraint fk_clientno_res references clients (clientno),
    park_code    varchar(2)
        constraint fk_park_code_res references cottagetypes (park_code)
        constraint HL_park_code check ( park_code = upper(park_code) ),
    typeno       varchar(5)
        constraint fk_typeno_res references cottagetypes (typeno)
        constraint HL_typeno check ( typeno = upper(typeno) ),
    houseno      numeric(3)
        constraint fk_houseno_res references cottages (houseno),
    booking_date date,
    start_date   date,
    end_date     date,
    reser_code   numeric(1) default null
        constraint dom_reser_code CHECK (reser_code IN (1, 2)),
    status       VARCHAR(6)
        CONSTRAINT dom_res_status CHECK (status in ('OPEN', 'PAID', 'CLOSED')),
    promo_code   varchar(9)
        constraint fk_prom_code references Promotions (promo_code),
    constraint pk_reservation primary key (resno, tano)
);
CREATE TABLE payments
(
    paymentNo      NUMERIC(9),
    resNo          NUMERIC(3),
    taNo           NUMERIC(2),
    payment_date   Date,
    amount         NUMERIC(8, 2),
    payment_method VARCHAR(1)
        CONSTRAINT dom_payment_method CHECK (payment_method IN ('V', 'M', 'O', 'B')),
    CONSTRAINT pk_payments PRIMARY KEY (paymentNo),
    CONSTRAINT fk_payment_reserv FOREIGN KEY (resNo, taNo) REFERENCES reservations (resNo, taNo)
);