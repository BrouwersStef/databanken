--OEF8
SELECT project_id, employee_id, hours
FROM tasks t
where hours < (select avg(hours)
               from tasks
               where t.project_id = project_id
               group by project_id)
ORDER BY 1;

--OEF10
select employee_id, first_name, last_name
from employees e
where EXISTS(
              select 'x'
              from employees
              where e.employee_id = manager_id
          );

create sequence seq_tano start with 5 increment 5;

select currval('seq_tano');

select nextval('seq_tano');

drop sequence seq_tano;


-- Creëer een view v_familieleden_projecten_maastricht die het volgende resultaat oplevert.
-- Geef employee_id, de naam en de leeftijd (in jaren) van de familieleden van de medewerkers die aan een project werken met locatie Maastricht.
-- Los op met 2 subqueries en 1 join.
-- Let op de gevraagde output. Je zal verschillende tekstfuncties moeten gebruiken om dit op te lossen.
-- Plak exact 5 * achter de naam van het familielid.


EMPLOYEE_ID | "naam familielid"	                     |"leeftijd familielid"
---------------------------------------------------------------------------
999555555   |Alex***** PARTNER of S. JOCHEMS         |44 jaar oud vandaag
999444444   |Andrew***** SON of W. ZUIDERWEG         |14 jaar oud vandaag
999444444   |Josefine***** DAUGHTER of W. ZUIDERWEG  |16 jaar oud vandaag
999444444   |Suzan***** PARTNER of W. ZUIDERWEG      |37 jaar oud vandaag

select
        e.employee_id,
        concat_ws(' ', rpad(name, length(name) + 5, '*'), relationship, 'of', "left"(e.first_name, 1) || '.',upper(e.last_name)) "naam familielid",
        date_part('YEAR', age(fm.birth_date)) || ' jaar oud vandaag' "leeftijd familielid"
from employees e
         join family_members fm on e.employee_id = fm.employee_id
where e.employee_id in (select distinct employee_id
                        from tasks t
                        where t.project_id = (select projects.project_id
                                              from projects
                                              where t.project_id = project_id
                                                and location = 'Maastricht'))


--alle emp die aan een proj in maasticht werken

select *
from projects



select distinct employee_id
from tasks t
where t.project_id = (select projects.project_id
                      from projects
                      where t.project_id = project_id
                        and location = 'Maastricht')