--deel1 subq

--oef1
select typeno, price_weekend
from cottype_prices
where season_code = 1
  and park_code = 'SF'
  and price_weekend > (select avg(price_weekend)
                       from cottype_prices
                       where season_code = 1
                         and park_code = 'SF');

--oef2
select *
from cottype_prices
where park_code = (select code
                   from parks
                   where initcap(park_name) = 'Weerterbergen')
  and price_weekend = (select max(price_weekend)
                       from cottype_prices
                       where park_code = (select code
                                          from parks
                                          where initcap(park_name) = 'Weerterbergen'));

--oef3
select park_name, cp.typeno, description, price_midweek
from cottype_prices cp
         join parks p on cp.park_code = p.code
         join seasons s on cp.season_code = s.code
         join cottagetypes c on c.park_code = cp.park_code and c.typeno = cp.typeno
where season_code in (select code
                      from seasons
                      where upper(description) like '%OFF%')
  and no_bedrooms = 3
  and price_midweek = (select max(price_midweek)
                       from cottype_prices cp
                                join cottagetypes c on c.park_code = cp.park_code and c.typeno = cp.typeno
                       where season_code in (select code
                                             from seasons
                                             where upper(description) like '%OFF%')
                         and no_bedrooms = 3)


--oef8
select park_name
from parks
where country_code = (select country_code
                      from countries
                      where initcap(country_name) = 'Netherlands')
and code in (select park_code
             from cottagetypes
             group by park_code
             having count(*)>=7)






