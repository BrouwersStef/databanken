select "current_schema"();
drop table DEPARTMENTS cascade;
create table DEPARTMENTS
(
    department_id   numeric(2)
        CONSTRAINT pk_department_id PRIMARY KEY,
    department_name varchar(20) not null,
    manager_id      char(9), --drop constraint
    mgr_start_date  date
);
drop table EMPLOYEES cascade;
create table EMPLOYEES
(
    employee_id   char(9) PRIMARY KEY,
    last_name     varchar(25) not null,
    first_name    varchar(25) not null,
    infix         varchar(25),
    street        varchar(50),
    city          varchar(25),
    province      char(2),
    postal_code   varchar(7),
    birth_date    date,
    salary        numeric(7, 2)
        check ( salary < 85000 ),
    parking_spot  numeric(4) unique,
    gender        char(1),
    department_id numeric(2)
        CONSTRAINT fk_depID references DEPARTMENTS (department_id),
    manager_id    char(9),
    CONSTRAINT fk_mgrID foreign key (manager_id) references EMPLOYEES (employee_id) --table constraint
);
drop table PROJECTS cascade;
create table PROJECTS
(
    project_id    numeric(2)
        constraint pk_proj_id primary key,
    project_name  varchar(25) not null,
    location      varchar(25),
    department_id numeric(2)
        constraint fk_loc references DEPARTMENTS (department_id)
);
drop table LOCATIONS cascade;
create table LOCATIONS
(
    department_id numeric(2)
        constraint fk_dep_id references DEPARTMENTS (department_id),
    location      varchar(20) not null,
    constraint pk_locations primary key (department_id, location)
);
drop table TASKS cascade;
create table TASKS
(
    employee_id char(9)
        constraint fk_emp_id references EMPLOYEES (employee_id),
    project_id  numeric(2)
        constraint fk_proj_id references PROJECTS (project_id),
    constraint pk_tasks primary key (employee_id, project_id)
);
drop table FAMILY_MEMBERS cascade;
create table FAMILY_MEMBERS
(
    employee_id  char(9)     not null
        constraint fk_empl_id references employees (employee_id),
    name         varchar(50) not null,
    gender       char(1)     not null,
    birth_date   date        not null
        constraint date_lim check ( birth_date between '1950/03/20' and '2018/01/01'), --DOE ALTIJD ISO FORMAAT YYYY/MM/DD
    relationship varchar(10) not null,
    constraint pk_fm primary key (employee_id, name)
)

