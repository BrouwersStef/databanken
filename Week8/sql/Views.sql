--OEF1
drop view v_emp_sal_dep;

create view v_emp_sal_dep as
select department_id,sum(salary)
from employees
group by department_id
order by 2;

select * from v_emp_sal_dep;

--OEF2
drop view v_emp_child;

create view v_emp_child as
select e.employee_id,concat_ws(' ',e.first_name,e.last_name),e.birth_date,name from family_members fm
                                    join employees e on fm.employee_id = e.employee_id
where upper(relationship) in ('SON','DAUGHTER')
order by birth_date,name;

select * from v_emp_child;
--OEF3
--A)
drop view v_emp_salary;

create view v_emp_salary as
select employee_id,first_name,last_name,salary
from employees
order by salary desc;

select * from v_emp_salary;
--B)
CREATE OR REPLACE VIEW v_emp_salary as
select employee_id,first_name,last_name,salary,department_id
from employees
order by salary desc;

select * from v_emp_salary;
/*
 Maak eerst view, dan voeg extra attribuut, nadien verwijdert het extra attribuut
 -> ERROR: je kunt geen attributen verwijderen.
 */
--OEF4
--A)
drop view v_department;

create view v_department as
    select * from departments;
--B)
alter table departments
add column dept_elnr numeric(9);
--C)
select * from v_department;
select * from departments;

SELECT view_definition
FROM INFORMATION_SCHEMA.views
WHERE table_name='v_department';
--D)
CREATE OR replace view v_department as
select * from departments;
--E)
alter table departments
drop column dept_telnr cascade;
--F)
CREATE OR replace view v_department as
select * from departments;
--5)
CREATE OR REPLACE VIEW v_emp_salary as
select employee_id,first_name,last_name,salary,department_id
from employees
where department_id = '7'
order by salary desc;
--6)
--!!!!!!!!!!!!!!
--7)
INSERT INTO v_emp_salary
VALUES('999999999','Jan','Janssens',35000,3);
select * from v_emp_salary
--idk ma view aanpassen i guess
CREATE OR REPLACE VIEW v_emp_salary as
select employee_id,first_name,last_name,salary,department_id
from employees
order by salary desc;
--8)
alter view v_emp_child rename to v_emp_child2;


select *
from v_emp_child2;
alter table employees drop column last_name;

--!!!!!!!!!!!!!!!
--9)
create view v_percentage_proportion as
select employee_id,project_id,round((hours/(select sum(hours)
                                            from tasks
                                            where project_id = t.project_id
                                            group by project_id))*100)||'%' "prec"
from tasks t
order by project_id,employee_id

select * from v_percentage_proportion








