--OEF1
select project_id from tasks where hours is not null group by project_id having count(employee_id) >= 3;

select project_id,project_name
from projects
where project_id in (select project_id from tasks where hours is not null group by project_id having count(employee_id) >= 3)
--OEF2
select project_id from projects where location ='Eindhoven';

select employee_id from tasks where project_id in (select project_id from projects where location ='Eindhoven')

select employee_id,last_name
from employees
where employee_id in (select tasks.employee_id from tasks where project_id in (select project_id from projects where location ='Eindhoven'))
order by 1 desc
--OEF3 A
select project_id from projects where upper(project_name) = 'ORDERMANAGEMENT'

select employee_id from tasks where project_id = (select project_id from projects where upper(project_name) = 'ORDERMANAGEMENT') and hours > 10


select first_name,last_name from employees
where employee_id in (select tasks.employee_id from tasks where project_id = (select project_id from projects where upper(project_name) = 'ORDERMANAGEMENT') and hours > 10)
--OEF3 B
select first_name,last_name
from employees
join tasks t on employees.employee_id = t.employee_id
join projects p on t.project_id = p.project_id
where upper(project_name)='ORDERMANAGEMENT' and hours > 10
--OEF4
select employee_id from family_members
where upper(relationship) in ('DAUGHTER','SON')
group by employee_id
having count(*) >=2

select employee_id,last_name
from employees
where employee_id in (select employee_id from family_members where upper(relationship) in ('DAUGHTER','SON') group by employee_id having count(*) >=2)
--OEF5
select department_id from employees
group by department_id
order by sum(salary) desc
fetch first 1 row only

select department_id,department_name from departments
where department_id = (select department_id from employees group by department_id order by sum(salary) desc fetch first 1 row only)
--OEF6
SELECT *
FROM EMPLOYEES
WHERE employee_id NOT IN (SELECT manager_id FROM EMPLOYEES);
--EMP die geen MGR ZIJN
--1)
select employee_id from employees
where employee_id not in (select distinct coalesce(manager_id,'' ) from employees)
--2)
select employee_id from employees
where employee_id not in (select distinct manager_id from employees where manager_id notnull)
--3) !!!!!!!!!!!!
select e.employee_id
from employees e
right outer join employees m on m.employee_id=e.manager_id
where e.employee_id!=m.manager_id

SELECT a.employee_id
FROM employees a
         LEFT OUTER JOIN employees b ON a.employee_id =  b.manager_id
WHERE b.employee_id is NULL
--4)
select employee_id
from employees e
where NOT exists (SELECT 'a' FROM employees WHERE e.employee_id = manager_id)

--OEF7
--BOCK
-- last_name _ aant kind _ comp _ last_name

select employee_id from employees where upper(last_name) ='BOCK'

--amount of kids for bock
select count (*) from family_members where upper(relationship) in ('SON','DAUGHTER') and employee_id = (select employee_id from employees where upper(last_name) ='BOCK')

--emp id met same amount of kids
select employee_id from family_members
where upper(relationship) in ('SON','DAUGHTER')
and employee_id != (select employee_id from employees where upper(last_name) ='BOCK')
group by employee_id
having count(*) = (select count (*) from family_members where upper(relationship) in ('SON','DAUGHTER') and employee_id = (select employee_id from employees where upper(last_name) ='BOCK'))

--name and shit from that emp
select e.employee_id, e.last_name, count(relationship)
from employees e
join family_members fm on e.employee_id = fm.employee_id
where e.employee_id in (select employee_id from family_members
                     where upper(relationship) in ('SON','DAUGHTER')
                       and employee_id != (select employee_id from employees where upper(last_name) ='BOCK')
                     group by employee_id
                     having count(*) = (select count (*) from family_members where upper(relationship) in ('SON','DAUGHTER') and employee_id = (select employee_id from employees where upper(last_name) ='BOCK')))
group by e.employee_id, e.last_name
--wing
select f.employee_id, e.last_name, count(relationship) aantal
from family_members f
join employees e on e.employee_id = f.employee_id
where lower(relationship) in ('son', 'daughter') and lower(last_name) != 'bock'
group by f.employee_id, e.employee_id
having count(relationship) = (select count(relationship) from family_members where lower(relationship) in ('son', 'daughter') and employee_id = (select employee_id from employees where lower(last_name) = 'bock')
group by employee_id);