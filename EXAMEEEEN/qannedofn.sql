-- VRAAG1
-- Maak een tabel aan, de tabel 'nationalities' met daarin volgende attributen:
--          - nat_id, een getal dat de primary key is.
--            Dit attribuut nummert automatisch en kan je zelf niet invullen bij het aanmaken van een nieuw record.
--            Het moet starten met tellen op 100. (Maak geen gebruik van sequences)
--          - nationality, een tekst van maximaal 30 characters.
-- Voeg de nationaliteit "Italian" toe aan de tabel nationalities.
-- Oplossing hier:

CREATE TABLE nationalities
(
    nat_id      INTEGER
        generated always as identity ( start with 100),
    nationality VARCHAR(30),
    constraint pk_nat primary key (nat_id)
);

insert into nationalities (nationality)
values ('Italian');
-- Tip:
SELECT *
FROM nationalities;

-- geeft:
-- nat_id   |   nationality
--  100     |    Italian

-- Voeg een extra veld nat_id toe aan de tabel actor.
--          - Zorg voor de koppeling tussen de tabel nationalities en actor.

-- Zorg dat alle acteurs die meespelen in de film "Chamber Italian" de Italiaanse nationaliteit krijgen.
--          - Let op! Je mag de gegevens niet zelf opzoeken, dit moet aan de hand van je queries gebeuren.

-- Oplossing hier:
alter table actor
    add column nat_id INTEGER,
    add constraint fk_nat_id foreign key (nat_id) references nationalities (nat_id);

-------------------
update actor
set nat_id = null;

update actor
set nat_id = 100
from film_actor fa
         join film f on fa.film_id = f.film_id
where f.title = 'Chamber Italian';


update actor
set nat_id = 100
where actor_id in (select a.actor_id
                   from actor a
                            join film_actor fa on a.actor_id = fa.actor_id
                            join film f on f.film_id = fa.film_id
                   where f.title = 'Chamber Italian')



-- TIP:
SELECT actor_id, first_name, last_name, nat_id
FROM actor
order by nat_id asc;

--geeft: --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- actor_id  |   first_name  |  last_name     |   nat_id
--   107     |   Gina        |  Degeneres     |     100
--   132     |   Adam        |  Hopper        |     100
--   133     |   Richard     |  Penn          |     100
--   148     |   Emily       |  Dee           |     100
--   29      |   Alec        |  Wayne         |     100
--   60      |   Henry       |  Berry         |     100
--   68      |   Rip         |  Winslet       |     100
--...
