-- VRAAG2

-- Deel 1:

-- Maak een view 'v_vraag2' van de filmen die meer dan 180 minuten duren.
-- Sorteer de films van langste naar kortste.
-- Via de view kan je geen rijen toevoegen of aanpassen van films die minder dan 180 minuten duren.
--      - Maw DML wordt tegengehouden en past niets aan in de tabel.
-- Let op de hoofding van de view

--oplossing hier: (check)
create or replace view v_vraag2 as
select film_id, title, rental_rate "price", length, language_id "l_code", rental_duration "r_time"
from film
where length >= 180
order by length desc
with check option;


--Output van:
SELECT *
FROM v_vraag2;
--geeft
-- film_id      title                 price        length       l_code       r_time
-- 690          Pond Seattle           2.99          185          1          7
-- 141          Chicago North          4.99          185          1          6
-- 182          Control Anthem         4.99          185          1          7
-- 212          Darn Forrester         4.99          185          1          7
-- 991          Worst Banger           2.99          185          1          4
-- 349          Gangs Pride            2.99          185          1          4
-- 426          Home Pity              4.99          185          1          7
-- 872          Sweet Brotherhood      2.99          185          1          3
-- 817          Soldiers Evolution     4.99          185          1          7
-- 609          Muscle Bright          2.99          185          1          7
-- 597          Moonwalker Fool        4.99          184          1          5
-- 820          Sons Interview         2.99          184          1          3
-- 886          Theory Mermaid         0.99          184          1          5
-- 180          Conspiracy Spirit      2.99          184          1          4
-- 499          King Evolution         4.99          184          1          3
-- 198          Crystal Breaking       2.99          184          1          6
-- 813          Smoochy Control        0.99          184          1          7
-- 821          Sorority Queen         0.99          184          1          6
-- 996          Young Language         0.99          183          1          6
-- 128          Catch Amistad          0.99          183          1          7
-- 340          Frontier Cabin         4.99          183          1          6
-- 767          Scalawag Duck          4.99          183          1          6
-- 973          Wife Turn              4.99          183          1          3
-- 721          Reds Pocus             4.99          182          1          7
-- 765          Saturn Name            4.99          182          1          7
-- 50           Baked Cleopatra        2.99          182          1          3
-- 774          Searchers Wait         2.99          182          1          3
-- 591          Monsoon Cause          4.99          182          1          6
-- 719          Records Zorro          4.99          182          1          7
-- 510          Lawless Vision         4.99          181          1          6
-- 473          Jacket Frisco          2.99          181          1          5
-- 467          Intrigue Worst         0.99          181          1          6
-- 841          Star Operation         2.99          181          1          5
-- 435          Hotel Happiness        4.99          181          1          6
-- 751          Runaway Tenenbaums     0.99          181          1          6
-- 406          Haunting Pianist       0.99          181          1          5
-- 24           Analyze Hoosiers       2.99          181          1          6
-- 974          Wild Apollo            0.99          181          1          4
-- 535          Love Suicides          0.99          181          1          6

-- Deel 2:

-- Toon aan met een DML instructie dat je view correct werkt
- Creer één werkende query en één niet werkende query.
       - maw je kan via de view enkel DML-instructies uitvoeren op tabel film voor rijen in de view.
       - maw ja kan rijen in de tabel film, maar buiten de view niet aanpassen.
;
-- Oplossing hier:
select *
from v_vraag2



-- Deel 3:
-- Creeer een nieuwe view 'v_vraag3b' op de gegevens van de view v_vraag2 met daarin:
--     - de films per prijs en hun huurtijd en de verhouding van deze.
--     - Let op je wenst enkel de films waarvan de huurtijd hoger is dan de gemiddelde huurtijd per prijs groep
--     - let op de sortering en titels
--     - Maak verplicht gebruik van een correlated subquery
;
-- oplossing hier:;

select *
from v_vraag2;


create or replace view v_vraag3b as
select price, r_time, round((price / r_time) * 100, 2) "relation percentage"
from v_vraag2
order by price desc, "relation percentage" desc



select *
from v_vraag2 v
where price in (select avg(r_time)
               from v_vraag2
               where v.price = price and
                     v.r_time = v.r_time
               group by price)

select price,avg(r_time)
from v_vraag2
where v.price = price
group by price
-- output van
SELECT *
from v_vraag3b;

-- geeft
--     price     r_time     relation percentage
--      4.99        7              71.29
--      2.99        5              59.80
--      2.99        6              49.83
--      2.99        7              42.71
--      0.99        6              16.50
--      0.99        7              14.14


