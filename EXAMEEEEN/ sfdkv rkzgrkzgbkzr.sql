-- VRAAG 3
-- De geboortedatums van Jon en Ed Chase zijn foutief in de actor tabel.
--     - Ze zijn een jaar later geboren dan de datum in de tabel.
--     - Bij elke aanpassing moet ook de last_update aangepast worden naar het moment dat je het aanpast.
--     - Zorg ervoor dat als je aan deze gegevens werkt er niemand anders aan kan werken.
--     - Als er op dit moment mensen aan deze gegevens werken willen we niet wachten tot zij klaar zijn
-- Wijzig de birthdates en de last_updated in 1 commando (!= 1 query)
-- Voer de wijzigingen effectief door.

-- Voer eerst volgende lijn uit:
\SET AUTOCOMMIT = OFF;

--oplossing hier:
select *
from actor
where upper(concat_ws(' ', first_name, last_name)) = 'JON CHASE'
   or upper(concat_ws(' ', first_name, last_name)) = 'ED CHASE'
for update;

update actor
set birthdate = birthdate + interval '1 year'
where upper(concat_ws(' ', first_name, last_name)) = 'JON CHASE'
   or upper(concat_ws(' ', first_name, last_name)) = 'ED CHASE'

update actor
set last_update = now()
where upper(concat_ws(' ', first_name, last_name)) = 'JON CHASE'
   or upper(concat_ws(' ', first_name, last_name)) = 'ED CHASE'

select *
from actor
where upper(concat_ws(' ', first_name, last_name)) = 'JON CHASE'
   or upper(concat_ws(' ', first_name, last_name)) = 'ED CHASE'
commit;


--output van:
select *
from actor
order by last_update DESC;
-- geeft:
--   nat_id     first_name  last_name              last_update                         birthdate          nat_id
--      3          Ed          Chase          2023-01-20 09:45:30.292927          2002-12-26          <null>
--    176          Jon         Chase          2023-01-20 09:45:30.292927          2000-10-29          <null>
--...